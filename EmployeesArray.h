#ifndef EMPLOYEES_ARRAY_H
#define EMPLOYEES_ARRAY_H

#include "Employee.h"
#include "IndexOutOfRangeException.h"

class EmployeesArray
{
	Employee** employees;
	int size;
public:
	EmployeesArray(int size);
	~EmployeesArray();
	void addEmployee(int index, Employee* newEmployee);
	Employee* getEmployee(int index);
};

#endif