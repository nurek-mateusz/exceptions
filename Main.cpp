#include <iostream>
#include "Employee.h"
#include "EmployeesArray.h"
#include "ReadOnlyException.h"
#include "IndexOutOfRangeException.h"

using std::cout;
using std::endl;
using std::cerr;

int main()
{
	const int size = 3;
	EmployeesArray employeesArray(size);
	
	employeesArray.addEmployee(0, new Employee("Jan", "Nowak", 1, 1500, true));
	employeesArray.addEmployee(1, new Employee("Krzysztof", "Kowalski", 2, 2700)); 
	employeesArray.addEmployee(2, new Employee("Piotr", "Nowicki", 1, 2100));

	try
	{
		for (int i = 0; i < size; i++)	// zmiana na size+1 spowoduje rzucenie wyjatku IndexOutOfRange
		{
			cout << "First name: " << employeesArray.getEmployee(i)->getFirstName() << endl;
			cout << "Last name: " << employeesArray.getEmployee(i)->getLastName() << endl;
			cout << "Department: " << employeesArray.getEmployee(i)->getDepartment() << endl;
			cout << "Salary: " << employeesArray.getEmployee(i)->getSalary() << endl << endl;
		}

		//employeesArray.getEmployee(0)->setDepartment(2);		// spowoduje rzucenie wyjatku ReadOnlyException
	}
	catch (ReadOnlyException e)
	{
		cerr << e.what();
	}
	catch (IndexOutOfRangeException e)
	{
		cerr << e.what();
	}
}