#ifndef INDEX_OUT_OF_RANGE_EXCEPTION_H
#define INDEX_OUT_OF_RANGE_EXCEPTION_H

#include <iostream>

using std::exception;
using std::cerr;

class IndexOutOfRangeException : public exception
{
public:
	const char* what() const
	{
		return "Index out of range!";
	}
};

#endif 