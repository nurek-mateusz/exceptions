#include "EmployeesArray.h"

EmployeesArray::EmployeesArray(int size)
{
	employees = new Employee*[size];
	this->size = size;
}

EmployeesArray::~EmployeesArray()
{
	for (int i = 0; i < size; i++)
	{
		delete employees[i];
	}
}

void EmployeesArray::addEmployee(int index, Employee* newEmployee)
{
	if (index < 0 || index >= size)
	{
		throw IndexOutOfRangeException();
	}

	employees[index] = newEmployee;
}

Employee* EmployeesArray::getEmployee(int index)
{
	if (index < 0 || index >= size)
	{
		throw IndexOutOfRangeException();
	}

	return employees[index];
}