#ifndef READ_ONLY_EXCEPTION_H
#define READ_ONLY_EXCEPTION_H

#include <iostream>

using std::exception;
using std::cerr;

class ReadOnlyException : public exception
{
public:
	const char* what() const
	{
		return "You try to modify read-only value!";
	}
};

#endif