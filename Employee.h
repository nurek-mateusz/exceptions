#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>
#include "ReadOnlyException.h"

using std::string;

class Employee
{
	string firstName;
	string lastName;
	int department;
	double salary;
	bool readOnly;
public:
	string getFirstName() { return firstName; }
	void setFirstName(string firstName) 
	{ 
		readOnly ? throw ReadOnlyException() : this->firstName = firstName; 
	}
	
	string getLastName() { return lastName; }
	void setLastName(string lastName)
	{ 
		readOnly ? throw ReadOnlyException() : this->lastName = lastName; 
	}
	
	int getDepartment() { return department; }
	void setDepartment(int department) 
	{
		readOnly ? throw ReadOnlyException() : this->department = department; 
	}
	
	double getSalary() { return salary; }
	void setSalary(double salary)
	{ 
		readOnly ? throw ReadOnlyException() : this->salary = salary; 
	}

	Employee(string firstName, string lastName, int department, double salary, bool readOnly = false)
	{
		this->firstName = firstName;
		this->lastName = lastName;
		this->department = department;
		this->salary = salary;
		this->readOnly = readOnly;
	}
};

#endif